import { Injectable } from '@angular/core';
import { Tour } from '../tour';
import { ajax } from 'rxjs/ajax';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DeleteService {

  private baseUrl:string = 'http://localhost:88/';
  constructor(private _http:HttpClient) { }

  deleteTour(tbNum:number){
    // return ajax.delete(this.baseUrl+'tour?tbNum='+tbNum,
    // {'Content-Type':'application/json','rxjs-custom-header':'Rxjs'});
    return this._http.delete(this.baseUrl+'tour?tbNum='+tbNum);
  } 
}
