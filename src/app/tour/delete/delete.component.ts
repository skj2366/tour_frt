import { Component, OnInit } from '@angular/core';
import { Tour } from '../tour';
import { DeleteService } from './delete.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TourService } from '../tour.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {

  tour:Tour = new Tour;
  tbNum:number;
  constructor(private _ds:DeleteService, private _router:ActivatedRoute,
    private _ts:TourService, private _routers:Router) { }

  ngOnInit() {
    console.log(this._router.paramMap);
    this._router.params.forEach(param=>{
      this.tbNum = param['tbNum'];
    })
  }

  // deleteTour(tour:Tour){
  //   // alert(this.tbNum);
  //   this._ds.deleteTour(this.tbNum).subscribe(res=>{
  //     // alert(res);
  //     console.log(res.response);
  //     this.goPage('/tour');
  //   })
  // }

  deleteTour(tour:Tour){
    if(confirm('삭제하시겠습니까?')==true){
      this._ds.deleteTour(this.tbNum).subscribe(res=>{
        //console.log(res.response);
        console.log(res);
        this.goPage('/tour');
      })
    }else{
      this.goPage('/tour');
      return;
    }
    
  }

  goPage(url:string){
    this._routers.navigate([url]);
  }
}
