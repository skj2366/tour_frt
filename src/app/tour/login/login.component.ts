import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { LoginService } from './login.service';
import { User } from './user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  us:User = new User();
  isLogin:boolean = false;
  constructor(private _ls:LoginService, private _router:Router) {
    
  }

  ngOnInit() {
  }
  doLogin():void{
    if(!this.us.uiId){
      alert('아이디를 입력해주세요.');
      return;
    }
    if(!this.us.uiPwd){
      alert('비밀번호를 입력해주세요.');
      return;
    }
    
    // this._ls.doLogin(this.us).subscribe(res=>{
    //   if(res.response){
    //     this.us = res.response;
    //     alert('로그인에 성공하였습니다.');
    //     this.isLogin = true;
    //     localStorage.setItem('uiId',this.us.uiId);
    //     this.goPage('/tour');
    //   }else{
    //     alert('아이디 비밀번호를 확인하세요');
    //   }
    //   console.log(res);
    // });

    this._ls.doLogin(this.us).subscribe(res=>{
      if(res){
        this.us = <User>res;
        if(this.us.tokken){
          localStorage.setItem('uiId',this.us.uiId);
          localStorage.setItem('tokken',this.us.tokken);
          console.log(localStorage.getItem('uiId'));
          console.log(localStorage.getItem('tokken'));
          alert('로그인 성공');
          this.goPage('');
        }
      }
    })

  }

  goPage(url:string){
    this._router.navigate([url]);
  }

}
