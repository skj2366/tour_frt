import { Injectable } from '@angular/core';
import { User } from './user';
import { ajax } from 'rxjs/ajax';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private baseUrl:string = 'http://localhost:88';

  constructor(private _http:HttpClient) { }

  doLogin(us:User){
    // return ajax.post(this.baseUrl + '/login',
    // {'uiId':us.uiId,'uiPwd':us.uiPwd},
    // {'Content-Type':'application/json','rxjs-custom-header':'Rxjs'});
    return this._http.post(this.baseUrl+'/login',us);
  }

  findList(){
    var uiId = localStorage.getItem('uiId');
    var tokken = localStorage.getItem('tokken');
    var param = '?uiId=' + uiId + '&tokken=' + tokken;
    return this._http.get(this.baseUrl+'/userinfos'+param);
  }

  signin(us:User){
    return this._http.post(this.baseUrl+'/join',us);
  }
}
