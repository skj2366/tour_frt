import { Component, OnInit } from '@angular/core';
import { Tour } from '../tour';
import { ActivatedRoute, Router } from '@angular/router';
import { TourService } from '../tour.service';
import { ajax } from 'rxjs/ajax';
import { TouchSequence } from 'selenium-webdriver';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  tbNum:number;
  tbLike:number;
  tbCnt:number;
  // tour:Tour;
  tour:{};
  private baseUrl:string = "http://localhost:88/";
  constructor(private _router:ActivatedRoute,
    private _ts:TourService, private _routers:Router, private _http:HttpClient) { }

  ngOnInit() {
    console.log(this._router.paramMap);

    this._router.params.forEach(param=>{
      this.tbNum = param['tbNum'];
      this.tbLike = param['tbLike'];
      this.tbCnt = param['tbCnt'];
    })

    this.addCnt(this.tbCnt).subscribe();
    this.tbCnt = this.tbCnt + 1;
    this._ts.getTourBoard(this.tbNum).subscribe(res=>{
      // console.log(res.response);
      // this.tour = res.response;
      console.log(res);
      this.tour = res;
    })

  }

  
  addCnt(tbCnt:number){
    // return ajax.put(this.baseUrl+'cnt?tbNum=' + this.tbNum ,tbCnt,
    // {'Content-Type':'application/json','rxjs-custom-header':'Rxjs'});
    return this._http.put(this.baseUrl + 'cnt?tbNum=' + this.tbNum, tbCnt);
  }

  goPage(url:string, tbNum:number){
    tbNum = this.tbNum;
    this._routers.navigate([url,tbNum]);
  }
}
