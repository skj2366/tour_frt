import { Injectable } from '@angular/core';
import { ajax } from 'rxjs/ajax';
import { Tour } from '../tour';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UpdateService {

  private baseUrl:string = 'http://localhost:88/';
  constructor(private _http:HttpClient) { }

  // updateTour(tour:Tour){
  //   return ajax.put(this.baseUrl+'tour',tour,
  //   {'Content-Type':'application/json','rxjs-custom-header':'Rxjs'});
  // }
  updateTour(tour:{}){
    return this._http.put(this.baseUrl+'tour',tour);
  }
  
}
