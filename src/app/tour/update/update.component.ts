import { Component, OnInit } from '@angular/core';
import { Tour } from '../tour';
import { UpdateService } from './update.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TourService } from '../tour.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  //tour:Tour = new Tour();
  // tour:Tour = new Tour();
  tour:{};
  tbNum:number;
  constructor(private _us:UpdateService, private _router:Router, private _routers:ActivatedRoute,
    private _ts:TourService) { }

  ngOnInit() {
    this._routers.params.forEach(param=>{
      this.tbNum = param['tbNum'];
    })

    this._ts.getTourBoard(this.tbNum).subscribe(res=>{
      // this.tour = res.response;
      this.tour = res;
    })
  }

  updateTour(tour:Tour){
    this._us.updateTour(this.tour).subscribe(res=>{
      // console.log(res.response);
      console.log(res);
      this.goPage('/tour');
    })
  }

  goPage(url:string){
    this._router.navigate([url]);
  }
}
