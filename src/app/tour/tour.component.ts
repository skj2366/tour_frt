import { Component, OnInit } from '@angular/core';
import { TourService } from './tour.service';
import { Router } from '@angular/router';
import { Tour } from './tour';
import { Kobiton } from 'protractor/built/driverProviders';

@Component({
  selector: 'app-tour',
  templateUrl: './tour.component.html',
  styleUrls: ['./tour.component.css']
})
export class TourComponent implements OnInit {
  // tourList:Tour[] = [];
  tourList:{};
  constructor(private _ts:TourService,private _router:Router) {
    this.findList(null);
  }
  findList(tour:Tour){
    this._ts.getTourBoardService(tour).subscribe(res=>{
       console.log(res);
       this.tourList = res;
      //  this.tourList = res.response;
    })
  }
  ngOnInit() {
    
  }

  goPage(url:string){
    // alert(url)
    this._router.navigate([url]);
  }
}
