import { Injectable } from '@angular/core';
import { Tour } from '../tour';
import { ajax } from 'rxjs/ajax';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InsertService {

  private baseUrl:string = 'http://localhost:88/';
  constructor(private _http:HttpClient) { }

  insertTour(tour:Tour){
    // return ajax.post(this.baseUrl+'inserttour',tour,
    // {'Content-Type':'application/json','rxjs-custom-header':'Rxjs'});
    return this._http.post(this.baseUrl+'inserttour',tour);
  }
}
