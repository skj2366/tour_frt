import { Component, OnInit } from '@angular/core';
import { Tour } from '../tour';
import { Router } from '@angular/router';
import { InsertService } from './insert.service';

@Component({
  selector: 'app-insert',
  templateUrl: './insert.component.html',
  styleUrls: ['./insert.component.css']
})
export class InsertComponent implements OnInit {

  tour:Tour = new Tour();
  constructor(private _is:InsertService, private _router:Router) { }

  ngOnInit() {
  }

  insertTour(tour:Tour){ 

    if(!this.tour.tbTitle){
      alert('제목을 입력하세요.');
      return;
    }
    if(!this.tour.tbContent){
      alert('내용을 입력하세요.');
      return;
    }
    if(!this.tour.tbWriter){
      alert('작성자를 입력하세요');
      return;
    }
    if(!this.tour.tbLoc){
      alert('지역을 입력하세요.');
      return;
    }
    if(!this.tour.tbScore){
      alert('점수를 입력하세요.');
      return;
    }

    this._is.insertTour(this.tour).subscribe(res=>{
      //console.log(res.response);
      console.log(res);
      this.goPage('/tour');
    })
  }

  goPage(url:string){
    this._router.navigate([url]);
  }

}
