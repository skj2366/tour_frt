import { Component, OnInit } from '@angular/core';
import { User } from '../../login/user';
import { SignService } from './sign.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { LoginService } from '../../login/login.service';

@Component({
  selector: 'app-sign',
  templateUrl: './sign.component.html',
  styleUrls: ['./sign.component.css']
})
export class SignComponent implements OnInit {

  us:User = new User();
  constructor(private _ss:SignService, private _router:Router, private _ls:LoginService) { }

  ngOnInit() {
  }

  doSign():void{
    if(!this.us.uiId){
      alert('아이디를 입력해주세요.');
      return;
    }
    if(!this.us.uiId){
      alert('비밀번호를 입력해주세요.');
      return;
    }
    if(!this.us.uiId){
      alert('나이를 입력해주세요.');
      return;
    }

    this._ss.doSign(this.us).subscribe(res=>{
      console.log(res);
    })
    
  }

  doSign2(){
    this._ls.signin(this.us).subscribe(res=>{
      console.log(res);
      if(res==1){
        alert('회원가입 성공');
        this._router.navigate(['/login']);
      }
    })
  }

}
