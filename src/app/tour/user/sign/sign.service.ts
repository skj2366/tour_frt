import { Injectable } from '@angular/core';
import { User } from '../../login/user';
import { ajax } from 'rxjs/ajax';

@Injectable({
  providedIn: 'root'
})
export class SignService {

  private baseUrl:string = 'http://localhost:88/';
  constructor() { }

  doSign(us:User){
    // return ajax.post(this.baseUrl + 'userinfo',
    // {'uiId':us.uiId,'uiPwd':us.uiPwd,'uiAge':us.uiAge},
    // {'Content-Type':'application/json','rxjs-custom-header':'Rxjs'});
    return ajax.post(this.baseUrl + 'userinfo',
    us,
    {'Content-Type':'application/json','rxjs-custom-header':'Rxjs'});
  }

}
