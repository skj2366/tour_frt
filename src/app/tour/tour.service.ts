import { Injectable } from '@angular/core';
import { ajax } from 'rxjs/ajax';
import { Tour } from './tour';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TourService {
  private baseUrl:string = 'http://localhost:88/';
  //private headers: HttpHeaders;
  
  constructor(private _http:HttpClient) { }

  getTourBoardService(tour:Tour){
    // return ajax.get(this.baseUrl + 'tours');
    //const headers = this.headers;
    return this._http.get(this.baseUrl+'tours');
  }

  getTourBoard(tbNum:number){
    //return ajax.get(this.baseUrl + 'tour?tbNum=' + tbNum);
    return this._http.get(this.baseUrl+'tour?tbNum=' + tbNum);
  }

}
