import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'tour_frt/src/app/tour/login/user';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
  @Input()
  cStr:string;
  @Input()
  cUser:any;
  @Output() issue = new EventEmitter<User>();
  us:User = new User();

  cTest:string = "abc";
  constructor() { }

  ngOnInit() {
  }
  test(){
    this.issue.emit(this.us);
  }

}
