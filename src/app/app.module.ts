import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TourComponent } from './tour/tour.component';

import { FormsModule } from '@angular/forms';
import { MenuComponent } from './menu/menu.component';

import { InsertComponent } from './tour/insert/insert.component';
import { LoginComponent } from './tour/login/login.component';
import { ViewComponent } from './tour/view/view.component';
import { DeleteComponent } from './tour/delete/delete.component';
import { UpdateComponent } from './tour/update/update.component';
import { SignComponent } from './tour/user/sign/sign.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptorService } from './auth-interceptor.service';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';

@NgModule({
  declarations: [
    AppComponent,
    TourComponent,
    LoginComponent,
    MenuComponent,
    ViewComponent,
    InsertComponent,
    DeleteComponent,
    UpdateComponent,
    SignComponent,
    ParentComponent,
    ChildComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
