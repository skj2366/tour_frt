import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TourComponent } from './tour/tour.component';
import { LoginComponent } from './tour/login/login.component';
import { InsertComponent } from './tour/insert/insert.component';
import { ViewComponent } from './tour/view/view.component';
import { DeleteComponent } from './tour/delete/delete.component';
import { UpdateComponent } from './tour/update/update.component';
import { SignComponent } from './tour/user/sign/sign.component';
import { RouterGuardService } from './router-guard.service';
import { ParentComponent } from './parent/parent.component';

const routes: Routes = [
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'tour',
    component : TourComponent,
    canActivate : [RouterGuardService]
  },
  // {
  //   path: '',
  //   redirectTo: '/login',
  //   pathMatch: 'full'
  // },//localhost로 첫페이지 접속시 자동으로 /login 페이지를 열게함.
  {
    path:'insert',
    component : InsertComponent,
    canActivate : [RouterGuardService]
    
  },
  {
    path:'view/:tbNum',
    component : ViewComponent,
    canActivate : [RouterGuardService]
  },
  {
    path:'delete/:tbNum',
    component : DeleteComponent,
    canActivate : [RouterGuardService]
  },
  {
    path:'update/:tbNum',
    component : UpdateComponent,
    canActivate : [RouterGuardService]
  },
  {
    path:'sign',
    component : SignComponent
  },
  {
    path:'parent',
    component : ParentComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
