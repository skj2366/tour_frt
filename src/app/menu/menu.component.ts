import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TouchSequence } from 'selenium-webdriver';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  isLogin:boolean = false;
  constructor(private _router:Router) { }

  ngOnInit() {
    if(localStorage.getItem('uiId')){
      this.isLogin = true;
    }
  }

  doLogout(){
    localStorage.clear();
    this._router.navigate(['login']);
    location.href='http://localhost/login';
  }

}